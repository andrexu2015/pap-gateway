#20191003
    前后端分离的项目，可以在部署过程中进行整合发布：

        在一些公司，部署实施人员的技术无法和互联网公司的运维团队相比，由于各种不定的环境也无法做到自动构建，容器化部署等。
    因此在这种情况下尽量减少部署时的服务软件需求，打出的包数量也尽量少。针对这种情况这里采用的在开发中做到前后端独立开发，
    打包时在后端springboot打包发布时将前端的构建输出一起打入，最后只需部署springboot的项目即可，无需再安装nginx服务器
    
    1）前端开发好后将build构建好的dist下的文件拷贝到springboot的resources的static下（boot项目默认没有static，需要自己新建）
        有可能会出现 /resources/static/static 这种形式的文件夹，第一个static 是手工创建的，第二个 static 是 vue 生成的。
        
    操作步骤：前端vue项目使用命令 npm run build 或者 cnpm run build 打包生成dist文件，在springboot项目中resources下建立static文件夹，
    将dist文件中的文件复制到static中，然后去application中跑起来boot项目，这样直接访问index.html就可以访问到页面
    （api接口请求地址自己根据情况打包时配置或者在生成的dist文件中config文件夹中的index.js中配置）
    
    2）
        第二种方法是在src/main下建立一个webapp文件夹，然后将前端项目的源文件复制到该文件夹下
        然后使用maven命令执行本地node打包命令,这样就可以 在执行mvn clean package命令时，利用maven插件执行cnpm run build命令
        (我是使用的淘宝的镜像cnpm,国外的npm命令会相对慢一些，大家根据自己的条件选择，具体命令请看项目中前端vue文件的README.md)，一次性完成整个过程
        
        实现方法是这样的，我们要引入org.codehaus.mojo插件来进行maven调用node命令，pom.xml中为：
        <plugin>
            <groupId>org.codehaus.mojo</groupId>
            <artifactId>exec-maven-plugin</artifactId>
            <executions>
                <execution>
                    <id>exec-cnpm-install</id>
                    <phase>prepare-package</phase>
                    <goals>
                        <goal>exec</goal>
                    </goals>
                    <configuration>
                        <executable>${cnpm}</executable>
                        <arguments>
                            <argument>install</argument>
                        </arguments>
                        <workingDirectory>${basedir}/src/main/webapp</workingDirectory>
                    </configuration>
                </execution>

                <execution>
                    <id>exec-cnpm-run-build</id>
                    <phase>prepare-package</phase>
                    <goals>
                        <goal>exec</goal>
                    </goals>
                    <configuration>
                        <executable>${cnpm}</executable>
                        <arguments>
                            <argument>run</argument>
                            <argument>build</argument>
                        </arguments>
                        <workingDirectory>${basedir}/src/main/webapp</workingDirectory>
                    </configuration>
                </execution>

            </executions>
        </plugin>
        　　
        
        然后maven-resources-plugin插件将项目的前端文件打包到boot项目的classes中，具体的请参考pom.xml中的，
        
         将webapp/dist文件夹中的文件复制到src/main/resources/static中，并打包到classes
        
        <!--copy文件到指定目录下 -->
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-resources-plugin</artifactId>
            <configuration>
                <encoding>${project.build.sourceEncoding}</encoding>
            </configuration>
            <executions>
                <execution>
                    <id>copy-spring-boot-webapp</id>
                    <!-- here the phase you need -->
                    <phase>validate</phase>
                    <goals>
                        <goal>copy-resources</goal>
                    </goals>
                    <configuration>
                        <encoding>utf-8</encoding>
                        <outputDirectory>${basedir}/src/main/resources/static</outputDirectory>
                        <resources>
                            <resource>
                                <directory>${basedir}/src/main/webapp/dist</directory>
                            </resource>
                        </resources>
                    </configuration>
                </execution>
            </executions>
        </plugin>
        然后通过maven命令：
        1
        mvn clean package -P window　
        打包成功后我们的前端项目就整个的打包到了boot项目的jar包中，然后启动项目，访问index.html页面就看到了项目启动成功。
        
#20201119
    Nginx 内容缓存：
        服务端的接口可以增加response.header :
            String headerValue = CacheControl.maxAge(10, TimeUnit.SECONDS).getHeaderValue();
            response.addHeader("Cache-Control", headerValue);
    
    http {
        # proxy_cache_path用来设置缓存的路径和配置，
        proxy_cache_path /home/admin/nginx/cache levels=1:2 keys_zone=nginx_cache:10m max_size=10g inactive=60m 
                     use_temp_path=off;
        
        server {
            listen       80;
            server_name  ngcache.alexgaoyh.com;
            
            # 显示当前页面的缓存状态，在 response 里面会有一个 X-Cache 的响应头
            add_header X-Cache $upstream_cache_status;
    
            location / {
                # proxy_cache用来启用缓存
                proxy_cache nginx_cache;
                proxy_pass http://10.10.7.242:8888;
                
                # 转发一些客户端信息
                proxy_set_header Host $host;
                proxy_set_header X-Real-IP $remote_addr;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header X-Forwarded-Proto $scheme;
                
                # 缓存键可以配置为任意值
                proxy_cache_key $proxy_host$request_uri$cookie_jessionid;
                # 这个命令定义了哪种类型的请求需要向服务器请求而不是尝试首先在缓存中查找。有些时候又被称作在内存中“打个洞”。
                proxy_cache_bypass $cookie_nocache $arg_nocache;
                # Nginx 内容缓存的一个非常强大的特性是：当无法从原始服务器获取最新的内容时，Nginx可 以分发缓存中的陈旧（stale，编者注：即过期内容）内容。只需要添加proxy_cache_use_stale命令即可
                # proxy_cache_use_stale 中的 updating 参数告知 Nginx 在客户端请求的项目的更新正在原服务器中下载时发送旧内容，而不是向服务器转发重复的请求。第一个请求陈旧文件的用户不得不等待文件在原服务器中更新完毕。陈旧的文件会返回给随后的请求直到更新后的文件被全部下载。
                proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504;
                # 当 proxy_cache_lock 被启用时，当多个客户端请求一个缓存中不存在的文件（或称之为一个MI SS），只有这些请求中的第一个被允许发送至服务器。其他请求在第一个请求得到满意结果之后在缓存中得到文件。如果不启用 proxy_cache_lock ，则所有在缓存中找不到文件的请求都会直接与服务器通信。
                proxy_cache_lock on;
            }
        }
    }