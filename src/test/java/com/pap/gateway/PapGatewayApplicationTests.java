package com.pap.gateway;

import com.pap.base.constant.JWTConstants;
import com.pap.base.dto.jwt.JWTUserDTO;
import com.pap.base.util.jwt.JWTTokenUtilss;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class PapGatewayApplicationTests {

    @Test
    public void contextLoads() {
        JWTUserDTO jwtUserDTO = new JWTUserDTO();
        jwtUserDTO.setUserId("1234567890");
        jwtUserDTO.setUserName("alexgaoyh");
        jwtUserDTO.setUserMobile("13800138000");
        jwtUserDTO.setDepartmentId("depId");
        jwtUserDTO.setTenantId("tenantId");

        List<String> permissionList = new ArrayList<String>();
        permissionList.add("1");
        permissionList.add("2");

        jwtUserDTO.setPermissionCodeList(permissionList);

        String token = null;
        try {
            token = JWTTokenUtilss.create(jwtUserDTO, "alexgaoyh1q2w!Q@W", JWTConstants.VALIDITY_TIME_MS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(token);

        System.out.println(JWTTokenUtilss.parse(token, "alexgaoyh1q2w!Q@W").toString());
    }


}
