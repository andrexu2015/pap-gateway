package com.pap.gateway.config.security.core.exception;

public class PapMalformedJwtException extends Exception {

    public PapMalformedJwtException() {
    }

    public PapMalformedJwtException(String message) {
        super(message);
    }

    public PapMalformedJwtException(Throwable cause) {
        super(cause);
    }

    public PapMalformedJwtException(String message, Throwable cause) {
        super(message, cause);
    }

    public PapMalformedJwtException(String message, Throwable cause,
                                  boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}