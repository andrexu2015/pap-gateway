package com.pap.gateway.config.security.core.exception;

public class PapEmptyJwtException extends Exception {

    public PapEmptyJwtException() {
    }

    public PapEmptyJwtException(String message) {
        super(message);
    }

    public PapEmptyJwtException(Throwable cause) {
        super(cause);
    }

    public PapEmptyJwtException(String message, Throwable cause) {
        super(message, cause);
    }

    public PapEmptyJwtException(String message, Throwable cause,
                                boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}