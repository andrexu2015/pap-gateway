package com.pap.gateway.config.security.core.exception.enums;

/**
 * 定义 JWT 异常枚举类
 */
public enum JWTExceptionEnum {

    PapEmptyJwtException(691, "JWT不存在"),

    PapExpiredJwtException(692, "JWT已过期"),

    PapIllegalArgumentException(693, "JWT非法参数"),

    PapMalformedJwtException(694, "JWT已被篡改"),

    PapSignatureException(695, "JWT验签失败"),

    PapUnsupportedJwtException(696, "JWT不支持的类型");

    private Integer code;
    private String msg;

    private JWTExceptionEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
