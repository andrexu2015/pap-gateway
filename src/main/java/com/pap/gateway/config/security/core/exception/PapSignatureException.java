package com.pap.gateway.config.security.core.exception;

public class PapSignatureException extends Exception {

    public PapSignatureException() {
    }

    public PapSignatureException(String message) {
        super(message);
    }

    public PapSignatureException(Throwable cause) {
        super(cause);
    }

    public PapSignatureException(String message, Throwable cause) {
        super(message, cause);
    }

    public PapSignatureException(String message, Throwable cause,
                                  boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}