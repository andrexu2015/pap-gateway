package com.pap.gateway.config.security.core.exception;

public class PapUnsupportedJwtException extends Exception {

    public PapUnsupportedJwtException() {
    }

    public PapUnsupportedJwtException(String message) {
        super(message);
    }

    public PapUnsupportedJwtException(Throwable cause) {
        super(cause);
    }

    public PapUnsupportedJwtException(String message, Throwable cause) {
        super(message, cause);
    }

    public PapUnsupportedJwtException(String message, Throwable cause,
                                  boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}