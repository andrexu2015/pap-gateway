package com.pap.gateway.config.security.core.exception;

public class PapIllegalArgumentException extends Exception {

    public PapIllegalArgumentException() {
    }

    public PapIllegalArgumentException(String message) {
        super(message);
    }

    public PapIllegalArgumentException(Throwable cause) {
        super(cause);
    }

    public PapIllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    public PapIllegalArgumentException(String message, Throwable cause,
                                  boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}