package com.pap.gateway.config.security.core.exception;

public class PapExpiredJwtException extends Exception {

    public PapExpiredJwtException() {
    }

    public PapExpiredJwtException(String message) {
        super(message);
    }

    public PapExpiredJwtException(Throwable cause) {
        super(cause);
    }

    public PapExpiredJwtException(String message, Throwable cause) {
        super(message, cause);
    }

    public PapExpiredJwtException(String message, Throwable cause,
                                  boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}