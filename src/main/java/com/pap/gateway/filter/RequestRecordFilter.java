package com.pap.gateway.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.util.MultiValueMap;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.Objects;
import java.util.UUID;

/**
 *  日志记录
 */
public class RequestRecordFilter implements GatewayFilter, Ordered {

    private static final Logger logger = LoggerFactory.getLogger(RequestRecordFilter.class);

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        URI requestUri = request.getURI();
        //Record only http requests (including https)
        String schema = requestUri.getScheme();
        if ((!"http".equals(schema) && !"https".equals(schema))){
            return chain.filter(exchange);
        }
        AccessRecord accessRecord = new AccessRecord();
        accessRecord.setPath(requestUri.getPath());
        accessRecord.setQueryString(request.getQueryParams());
        accessRecord.setMethod(request.getMethod().name());
        accessRecord.setIp(request.getRemoteAddress().getHostName());
        accessRecord.setUserAgent(request.getHeaders().getFirst("User-Agent"));
        exchange.getAttributes().put("startTime", System.currentTimeMillis());
        exchange.getAttributes().put("requestIdFilter", UUID.randomUUID().toString());

        String method = request.getMethodValue();
        String contentType = request.getHeaders().getFirst("Content-Type");

        // body
        try {
            String body = exchange.getAttribute("cachedRequestBodyObject");
            accessRecord.setBody(body);
        } catch (Exception e) {
            logger.error("RequestRecordFilter-body : [{}]", e.getMessage());
            e.printStackTrace();
        }
        //

        return returnMono(chain, exchange, accessRecord);
    }

    private Mono<Void> returnMono(GatewayFilterChain chain,ServerWebExchange exchange, AccessRecord accessRecord){
        return chain.filter(exchange).then(Mono.fromRunnable(()->{
            Long startTime = exchange.getAttribute("startTime");
            if (startTime != null){
                long executeTime = (System.currentTimeMillis() - startTime);
                accessRecord.setExpendTime(executeTime);
                accessRecord.setHttpCode(Objects.requireNonNull(exchange.getResponse().getStatusCode()).value());
                logger.info("[{}][{}][{}][{}][{}][{}][{}][{}][{}]",
                        accessRecord.getMethod(),
                        accessRecord.getPath(),
                        accessRecord.getIp(),
                        accessRecord.getUserAgent(),
                        exchange.getResponse().getHeaders().getContentType() ,
                        exchange.getAttribute("requestIdFilter"),
                        executeTime,
                        accessRecord.getHttpCode(),
                        accessRecord.getBody());
            }
        }));
    }

    @Override
    public int getOrder() {
        return 1;
    }

    /**
     * Access Record Objects
     */
    private class AccessRecord{
        private String method;
        private String ip;
        private String userAgent;
        private String path;
        private String body;
        private MultiValueMap<String,String> queryString;
        private long expendTime;
        private int httpCode;

        public String getMethod() {
            return method;
        }

        public void setMethod(String method) {
            this.method = method;
        }

        public String getIp() {
            return ip;
        }

        public void setIp(String ip) {
            this.ip = ip;
        }

        public String getUserAgent() {
            return userAgent;
        }

        public void setUserAgent(String userAgent) {
            this.userAgent = userAgent;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public MultiValueMap<String, String> getQueryString() {
            return queryString;
        }

        public void setQueryString(MultiValueMap<String, String> queryString) {
            this.queryString = queryString;
        }

        public long getExpendTime() {
            return expendTime;
        }

        public void setExpendTime(long expendTime) {
            this.expendTime = expendTime;
        }

        public int getHttpCode() {
            return httpCode;
        }

        public void setHttpCode(int httpCode) {
            this.httpCode = httpCode;
        }
    }

}