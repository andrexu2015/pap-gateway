package com.pap.gateway.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public class HeaderFilter implements GatewayFilter, Ordered {

    private static final Logger logger = LoggerFactory.getLogger(HeaderFilter.class);

    /**
     * 全局过滤器 核心方法
     *
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        String papToken = exchange.getRequest().getHeaders().getFirst("papToken");
        // TODO 这里从 header:papToken 中获取到当前登录用户的 token 信息，可以解析为具体的登录用户信息，
        //  可以在这里针对 灰度发布做文章，确定了用户，即可以确定当前需要跳转到哪里，增加 header 信息，后续的服务端根据 header 进行处理。
        //向headers中放文件，记得build， 这里注意是否将原先的 header 的数据删除(按需处理)
        ServerHttpRequest host = exchange.getRequest().mutate()
                .headers(httpHeaders -> httpHeaders.remove("papVersion"))
                .header("papVersion", "1.2.3").build();
        //将现在的request 变成 change对象
        ServerWebExchange build = exchange.mutate().request(host).build();
        return chain.filter(build);
    }

    @Override
    public int getOrder() {
        return -200;
    }

}
